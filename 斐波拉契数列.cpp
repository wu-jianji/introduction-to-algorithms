#include<stdio.h>
int Feipola(int n)
{
	if (n<=2)
	{
		return 1;
	}
	else
	return  Feipola(n - 1) + Feipola(n - 2);
}
int main()
{
	int n, i;
	i = Feipola(7);
	printf("斐波拉契数列的第7个数为：%d", i);
	return 0;
}
//时间复杂度为O（2^n），空间复杂度我O（1）

#include<stdio.h>
int Feibola(int n)
{
	if (n <= 2)
	{
		return 1;
	}
	int f1 = 1, f2 = 1, i, f;
	for (i = 3; i <= n; ++i)
	{
		f = f1 + f2;
		f1 = f2;
		f2 = f;
	}
	return f;
}
int main()
{
	int n,k;
	k = Feibola(7);
	printf("斐波拉契数列的第7个数为：%d", k);
	return 0;
}
//时间复杂度为O（n），空间复杂度我O（1）