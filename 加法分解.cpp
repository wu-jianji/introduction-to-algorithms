#include<iostream>
using namespace std;
int sum;
void dfs(int n, int count, int* p)
{
	if (n == 0)
	{
		cout << sum << "=";
		for (int i = 0; i < count; i++)
		{
			if (i == count - 1)
			{
				cout << p[i] << endl;
			}
			else
			{
				cout << p[i] << "+";
			}
		}
		return;
	}
		int k = count > 0 ? p[count - 1] : 1;
		for (int i = k; i <= n; i++)
		{
			p[count] = i;
			dfs(n - i, count + 1, p);
		}
	
}
int main()
{
	int n, m;
	cin >> n >> m;
	sum = n;
	int* p = new int[n];
	if (m == 1)
		dfs(n, 0, p);
	return 0;
}