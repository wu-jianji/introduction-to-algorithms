#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#define MAXN 20
using namespace std;
int pre[MAXN];
int num = 0;
int Num=4;
void Output(int pre[], int col) {
    for (size_t num = 0; num < col; ++num)
    {
        for (size_t i = 0; i < col; ++i)
        {
           if (pre[num] == i)
           {
               cout << "X";
           }
           else {
               cout << "O";
           }
           
        }
        cout << endl;
    }
    cout << endl;
    num++; 
}
bool check(int pre[], int col){
      for(int i = 0; i < col; i++){          
	  if(pre[i] == pre[col] || abs(pre[i] - pre[col]) == col - i){  
		return false;
	 }
     }
     return true;
}
void dfs(int pre[], int col){
	if(col == Num){           
		Output(pre,col);
		return ;
	}
	for(int i = 0; i < Num; i++){   
		pre[col] = i;                
		if(check(pre,col)){            
			dfs(pre,col+1);           
		}
	}
}
int main()
{
	memset(pre,0,sizeof(pre));
	dfs(pre,0);                  
	printf("总方案数：%d\n",num);
	return 0;
}