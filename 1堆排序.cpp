#include <iostream>

using namespace std;

//堆排序的核心思想
class BigTopHeap {
    private:
        int *array; //存储堆的数组，从下标1开始存储数据
        int capacity; //堆中可以存储的数据的个数
        int count; //当前堆中已经存储的数据的个数

        void heapifyFromTopPos(int pos) {
            while (true)
            {
                int maxPos = pos;      //父节点 
                int curPos = pos * 2;         //子左节点 
                if (curPos < count && array[pos] < array[curPos]) {   //判断父节点大还是子左节点大 
                    maxPos = curPos;             //找出最大的数 
                }
                ++curPos;          //子右节点 
                if (curPos < count && array[maxPos] < array[curPos]) {    //判断右节点是不是最大的 
                    maxPos = curPos;            //找出最大的数 
                }

                if (pos == maxPos) {
                    //没有发生交换，说明当前位置的数据均比其子节点大
                    break;
                }
                int temp = array[pos];                //将最大的数交换至父节点 
                array[pos] = array[maxPos];
                array[maxPos] = temp;
                pos = maxPos;
            }
        }

    public:
        // array一定是从下标1开始放数据
        BigTopHeap(int *arr, int arrayCapacity) {       
            array = arr;
            capacity = arrayCapacity;
            count = capacity;

            for (int i = count/2; i > 0; --i) {       //建堆 
                heapifyFromTopPos(i);
            }
        }

        void insert(int data) {      //把数据插入堆 
            if (count >= capacity) {
                return;
            }

            array[++count] = data;    
            int i = count;         //count当前堆已经存储好的数据的个数 
            while (i/2 > 0 && array[i] > array[i/2]) {
                int temp = array[i];
                array[i] = array[i/2];
                array[i] = temp;
                i = i/2;
            }
        }

        int removeTop() {          
            int topValue = 0;             //移动出的堆头数的个数 
            if (0 == count) {
                return topValue;
            }

            topValue = array[1];        

            array[1] = array[count];      //头尾互换 
            --count;
            heapifyFromTopPos(1);      //把堆重新排序 
            return topValue;
        }

        int* sortHeap() {          //堆排序 
            while(count > 0) {
                int topValue = removeTop();
                array[count + 1] = topValue;
            }

            return array;
        }

};
void printArray(int array[], int arrLen) {
    for (int i = 1; i < arrLen; ++i) {
        cout << array[i] << " ";
    }
    cout << endl;
}

int main(){
    int array0[] = {0};
    int arrayLen = sizeof(array0)/sizeof(int);

    printArray(array0, arrayLen);
    BigTopHeap h0(array0, arrayLen - 1);
    h0.sortHeap();
    printArray(array0, arrayLen);

    cout << "=========================================" << endl;

    int array1[] = {0,1};
    arrayLen = sizeof(array1)/sizeof(int);

    printArray(array1, arrayLen);
    BigTopHeap h1(array1, arrayLen - 1);
    h1.sortHeap();
    printArray(array1, arrayLen);

    cout << "=========================================" << endl;

    int array2[] = {0, 2, 1};
    arrayLen = sizeof(array2)/sizeof(int);

    printArray(array2, arrayLen);
    BigTopHeap h2(array2, arrayLen - 1);
    h2.sortHeap();
    printArray(array2, arrayLen);

    cout << "=========================================" << endl;

    int array3[] = {0, 1, 5, 3};
    arrayLen = sizeof(array3)/sizeof(int);

    printArray(array3, arrayLen);
    BigTopHeap h3(array3, arrayLen - 1);
    h3.sortHeap();
    printArray(array3, arrayLen);


    cout << "=========================================" << endl;

    int array4[] = {0, 9, 12, 8, 7};
    arrayLen = sizeof(array4)/sizeof(int);

    printArray(array4, arrayLen);
    BigTopHeap h4(array4, arrayLen - 1);
    h4.sortHeap();
    printArray(array4, arrayLen);

    cout << "=========================================" << endl;

    int array5[] = {0, 9, 12, 8, 7, 3};
    arrayLen = sizeof(array5)/sizeof(int);

    printArray(array5, arrayLen);
    BigTopHeap h5(array5, arrayLen - 1);
    h5.sortHeap();
    printArray(array5, arrayLen);


}
