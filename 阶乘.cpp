#include<stdio.h>
int Factorial(int n)
{
	if (n == 1 || n == 2)
	{
		return n;
	 }
	else
    {
		return n * Factorial(n - 1);
	}
	return n;
}
int main()
{
	int n, i;
	i = Factorial(10);
	printf("%d", i);
	return 0;
}
//时间复杂度为O（n），空间复杂度我O（1）


#include<stdio.h>
int Factorial(int n)
{
	if (n == 1)
	{
		return n;
	}
	else 
	{
		int result = 1;
		for (int i=1;i<=n;++i)
	   {
			 result = result * i;
		}
		return result;
	}
}
int main()
{
	int n, j;
	j = Factorial(10);
	printf("%d", j);
	return 0;
}
//时间复杂度O（n），空间复杂度O(1);