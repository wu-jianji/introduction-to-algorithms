#include <iostream>

using namespace std;

void printArray(int array[], int arrLen) {
    for (int i = 0; i < arrLen; ++i) {
        cout << array[i] << " ";
    }
    cout << endl;
}
//查找第一个值等于给定值的元素 
int binarySearchFirstEqual(int searchKey, int array[], int arrLen) {
   int low = 0;          //定义左边第一个数 
   int high = arrLen - 1;  //定义右边第一个数 
   int keyPos = -1;     //定义关键数 

   while (low <= high) {         //当遍历完后退出循环的条件 
        int mid = low +((high - low) >> 1);    //找到数组中的中间数 
        if (array[mid] > searchKey) {          //如果中间的数大于要搜索的值 
            high = mid - 1;         //接下来只需要遍历左边第一个数到中间数的这个数列，将中间的值变成需要遍历的末位值 
        }
        else if (array[mid] < searchKey){    //如果要搜索的数大于中间值 
            low = mid + 1;   // 接下来只需要遍历中间数到右边第一个值的这个数列，将左边第一个值变成需要遍历数列的第一个值即中间值 
        }
        else {
            if (mid == 0 || array[mid - 1] != searchKey) {    //如果中间值为左边第一个数 且 数组中间值往前一个数不等于要搜索的值 
                keyPos = mid;       //中间值为要搜索的值即第一个值为给定元素 
                break;
            }
            else {
                high = mid - 1;
            }
        }
   }

   return keyPos;
}
//查找最后一个值等于给定元素 
int binarySearchLastEqual(int searchKey, int array[], int arrLen) {
   int low = 0;      //定义左边第一个数 
   int high = arrLen - 1;     //定义右边第一个数 
   int keyPos = -1;        //定义关键数 

     while (low <= high) {     //当遍历完后退出循环的条件 
        int mid = low +((high - low) >> 1);     //找到数组中的中间数 
        if (array[mid] > searchKey) {          //如果中间的数大于要搜索的值 
            high = mid - 1;      //接下来只需要遍历左边第一个数到中间数的这个数列，将中间的值变成需要遍历的末位值 
        }
        else if (array[mid] < searchKey){      //如果要搜索的数大于中间值 
            low = mid + 1;         // 接下来只需要遍历中间数到右边第一个值的这个数列，将左边第一个值变成需要遍历数列的第一个值即中间值 
        }
        else {
            if (mid == arrLen - 1 || array[mid + 1] != searchKey) {     //如果中间值为右边第一个数 且 数组中间值往后一个数不等于要搜索的值 
                keyPos = mid;                 //中间值为要搜索的值即最后一个值为给定元素 
                break;
            }
            else {
                low = mid + 1;
            }
        }
    }

   return keyPos;
}
//查找第一个大于给定元素的值 
int binarySearchFirstLargerEqual(int searchKey, int array[], int arrLen) {
   int low = 0;        //定义左边第一个数 
   int high = arrLen - 1;      //定义右边第一个数 
   int keyPos = -1;      //定义关键数 

     while (low <= high) {          //当遍历完后退出循环的条件 
        int mid = low +((high - low) >> 1);        //找到数组中的中间数
        if (array[mid] >= searchKey) {       //如果中间的数大于等于要搜索的值 
            if (mid == 0 || array[mid - 1] < searchKey) {       //mid等于数组第一个数 且 要查找的值大于mid往前的一个数 
                keyPos = mid;     //第一个大于给定元素的值 
                break;
            }
            else {
                high = mid - 1;
            }
            
        }
        else {
            low = mid + 1;
        }
    }

   return keyPos;
}
//查找最后一个元素小于给定值的元素 
int binarySearchLastSmallerEqual(int searchKey, int array[], int arrLen) {
   int low = 0;      //定义左边第一个数 
   int high = arrLen - 1;        //定义右边第一个数 
   int keyPos = -1;      //定义关键数 

     while (low <= high) {         //当遍历完后退出循环的条件 
        int mid = low +((high - low) >> 1);     //找到数组中的中间数
        if (array[mid] <= searchKey) {       //如果要搜索的数大于等于中间值 
            if (mid == arrLen - 1 || array[mid + 1] > searchKey) {  //mid值等于最后一个数 且 mid往后一个值大于要搜索的值 
                keyPos = mid;  //最后一个元素小于给定值的元素 
                break;
            }
            else {
                low = mid + 1;
            }
            
        }
        else {
            high = mid - 1;
        }
    }

   return keyPos;
}



int main(){
    int array0[] = {1, 1, 2, 3, 4, 5, 6};
    int arrayLen = sizeof(array0)/sizeof(int);

    printArray(array0, arrayLen);
    cout << "First Array Pos of 1: "<< binarySearchFirstEqual(1, array0, arrayLen) << endl;
    cout << "Last Array Pos of 1: "<< binarySearchLastEqual(1, array0, arrayLen) << endl;
    cout << "First Larger Equal Array Pos of 1: "<< binarySearchFirstLargerEqual(1, array0, arrayLen) << endl;
    cout << "Last Smaller Equal Array Pos of 1: "<< binarySearchLastSmallerEqual(1, array0, arrayLen) << endl;

    cout << "=========================================" << endl;

    int array1[] = {1, 2, 2, 3, 3, 5, 6, 8};
    arrayLen = sizeof(array1)/sizeof(int);
   
    printArray(array1, arrayLen);
    cout << "First Array Pos of 3: "<< binarySearchFirstEqual(3, array1, arrayLen) << endl;
    cout << "Last Array Pos of 3: "<< binarySearchLastEqual(3, array1, arrayLen) << endl;
    cout << "First Larger Equal Array Pos of 3: "<< binarySearchFirstLargerEqual(3, array1, arrayLen) << endl;
    cout << "Last Smaller Equal Array Pos of 3: "<< binarySearchLastSmallerEqual(3, array1, arrayLen) << endl;

    cout << "=========================================" << endl;

    int array2[] = {1, 2, 2, 3, 3, 5, 6, 8};
    arrayLen = sizeof(array2)/sizeof(int);

    printArray(array2, arrayLen);
    cout << "First Array Pos of 4: "<< binarySearchFirstEqual(4, array2, arrayLen) << endl;
    cout << "Last Array Pos of 4: "<< binarySearchLastEqual(4, array2, arrayLen) << endl;
    cout << "First Larger Equal Array Pos of 4: "<< binarySearchFirstLargerEqual(4, array2, arrayLen) << endl;
    cout << "Last Smaller Equal Array Pos of 4: "<< binarySearchLastSmallerEqual(4, array2, arrayLen) << endl;

    cout << "=========================================" << endl;
}
