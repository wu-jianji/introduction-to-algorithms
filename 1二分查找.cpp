#include<stdio.h>
int binarySearch(int nums[], int key,int numsLen) {
    int left = 0;  
    int right = numsLen -1; 
 
    while(left <= right) { 
        int mid = (right + left) / 2;
        if(nums[mid] == key)
            return mid; 
        else if (nums[mid] < key)
            left = mid + 1; 
        else if (nums[mid] > key)
            right = mid - 1; 
        }
    return -1;
}
int main()
{
	int nums[10]={1,2,3,4,5,6,7,8,9,10,};
	int numsLen = sizeof(nums)/sizeof(int);
	int key;
	printf("输入你要查找的值:");
	scanf("%d",&key);
	if(binarySearch(nums,key,numsLen)==-1)
	{
		printf("没有找到该值");
	}
	else
	printf("查找到你要查找到的值：%d",nums[binarySearch(nums,key,numsLen)]);
	return 0;
} 





最好情况为O（1）
最坏情况O(logn)