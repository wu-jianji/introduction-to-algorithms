#include<stdio.h>
int fun1(int n)
{
	if (n == 1)
	{
		return 1;
	}
	else if (n == 2)
	{
		return 2;
	}
	else 
	{
		return fun1(n - 1) + fun1(n - 2);
	}
}
int main()
{
	int n;
	int i;
	i=fun1(10);
	printf("%d", i);
	return 0;
}  
//时间复杂度为O（2^n），空间复杂度我O（1）

#include<stdio.h>
int fun1(int n)
{
	if (n == 1)
	{
		return 1;
	}
	else if (n == 2)
	{
		return 2;
	}
	else 
	{
		return fun1(n - 1) + fun1(n - 2);
	}
}
int main()
{
	int n;
	int i;
	i=fun1(10);
	printf("%d", i);
	return 0;
}  
//时间复杂度为O（n），空间复杂度我O（1）