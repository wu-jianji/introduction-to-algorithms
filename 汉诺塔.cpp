#include<iostream>
using namespace std;
void move(char x, char y)
{
	cout << x << "-->" << y << endl;
}
void Hanoi(int n, char A, char B, char C)
{
	if (n == 1)
	{
		move(A, C);
	}
	else
	{
		Hanoi(n - 1, A, C, B);
		move(A, C);
		Hanoi(n - 1, B, A, C);
	}
}
int main()
{
	int n;
	cout << "输入盘子的数量" << endl;
	cin >> n;
	Hanoi(n, 'A', 'B', 'C');
	return 0;
}
//时间复杂度为O（2^n），空间复杂度为O（1）