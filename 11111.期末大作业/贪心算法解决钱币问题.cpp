#include<iostream>
#include<algorithm>
using namespace std;
#define N 7
int Count[N] = {1,2,5,10,20,50,100 };
int Value[N] = { 1,2,5,10,20,50,100 };
int test(int k)
{
    int num = 0;
    for (int i = N - 1; i >= 0; i--)
    {
        int c;
        c = min(k / Value[i], Count[i]);  //money / value[i]得到钱数的张数，最大张数为Count[i]
        k = k - c * Value[i];
        num += c;
    }
    if (k > 0)
        cout << "零钱找不开"<<endl;
    else
        cout << "零钱找得开"<<endl;

    return num;
}

int main()
{
    int k;
    cin >> k;
    int small = test(k);
    cout << "最少需要多少张：" << small;
}