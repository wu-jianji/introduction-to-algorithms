#include <iostream>
using namespace std;
int partition(int a[], int left, int right) {
    int x = a[left];   //设置左边第一个数为基准数
    while (left < right)  //当数组遍历完就退出循环
     {         
        while (left < right && a[right] >= x)     //当基准数小于或等于右边不断遍历向左边的数时退出循环
        {
            right--;
        }
        if (left < right) 
        {
            a[left] = a[right];
            left++;
        }
        while (left < right && a[left] < x)       //当基准数大于左边不断遍历向右边的数时退出循环
        {
            left++;
        }
        if (left < right)      
        {
            a[right] = a[left];
            right--;
        }
    }
    a[left] = x;
    return left;
}
int main() {
    int a[10] = { 10, 8, 3, 5, 6, 2, 1, 7, 9, 44 };
    int aLen = sizeof(a) / sizeof(int);
    int k = 10;
    int  left=0;//最左边的数充当哨兵
    int  right=9;//最右边的数充当哨兵
    int p=partition(a, left, right);
    while (p != k - 1)
    {
        //如果返回的这个值比k小，则在后面部分进行调用quicksort
        if (p < k - 1)
        {
            left = p + 1;
            p = partition(a, left, right);
        }
        else
        {
            //如果返回的这个值比k大，则在前面部分进行调用quicksort
            right = p - 1;
            p = partition(a, left, right);
        }
    }
    cout << a[p] << endl;
    return 0;