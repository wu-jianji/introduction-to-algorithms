#include<iostream>
using namespace std;
int dpk(int k, int* value, int count)
{
    int* dparray = new int[k + 1]();  //建立动态规划数组
    for (int i = 0; i < k + 1; i++)
    {
        dparray[i] = i;         
    }
    int mincount = 0;   //定义最少需要的数量
    for (int i = 0; i < count; i++)
    {
        int curk = value[i];      //对应行的初始价值
        for (int j = 1; j <= k; ++j)
        {
            int currentcount = 0;    //目前行存储零钱需要的最少数量
            if (j < curk)       //如果当前行数的钱小于该行初始的钱币价值
            {              
                currentcount = dparray[j];      //将此时的最少张数记录下来
            }
            else 
            {
                currentcount = j / curk;      //当前钱币价值刚好被分完
                int letfPayment = j % curk;   //经过前面还剩余的钱
                if (letfPayment > 0)           //如果还剩余钱
                {
                    currentcount += dparray[letfPayment];   //将之前的最少张数和剩余价值所要的最少张数相加
                }
            }
            //找出需要的最少张数
            if (currentcount < dparray[j]) 
            {
                dparray[j] = currentcount;   //将最少张数记录下来
            }
        }
    }
    mincount = dparray[k];//将最少张数的值赋给mincount
    delete[] dparray;
    return mincount;
}
int main()
{
    int k, mincount = 0;
    cin >> k;
    int value[7] = { 1,2,5,10,20,50,100 };  //存储纸币币值
    int count = sizeof(value) / sizeof(int);   //计算纸币数量
    mincount = dpk(k, value, count);    
    if (mincount == 0)    
    {
        cout << "不能支付";
    }
    else
        cout << "最少需要" << mincount<<"张纸币";
    return 0;
}
