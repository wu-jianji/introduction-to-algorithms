

/*
#include<stdio.h>
int a[100],n;
void quicksort(int key,int n)
{
	int i,j,temp;
	if(key>n)   //根据我的定义，如果我输入的数的值比我的数组的个数的数量大，则返回 
	return;
	temp=a[key];   //存储基准数 
	i=key;    //i为基准数的下标 
	j=n;    //j为数组最后一个数的下标 
	while(i != j)    //如果i与j的位置相同 则跳出循环 
	{
		while(a[j]>=temp && i<j)    //如果j大于基准数，则j一直向左前移动 ，但不能超过从左边i的位置 
		j--;
		while(a[i]<=temp && i<j)    //如果i小于基准数，则i一直向右前移动 ，但不能超过从右边j的位置 
		i++;
		if(i<j)                    //如果他们没有相遇，就交换他们的位置 
		{
			int t;
			t=a[i];
			a[i]=a[j];
			a[j]=t;
		}
	}
	a[key]=a[i];     //将找到准确位置的数放入该位置 
	a[i]=temp;       //交换位置 
	quicksort(1,i-1);    //处理其左边的数 
	quicksort(i+1,n);   //处理其右边的数 
	return;
}
int main()
{
	int i,sum,k;
	printf("输入一个数确定该数组有多少个数:");
	scanf("%d",&n);
	for(i=1;i<=n;i++)       
	{
		scanf("%d",&a[i]);
	}
	quicksort(1,n);  //将数组的第一个值作为基准数，方便查找 
	printf("输入一个数k：");
	scanf("%d",&k);
	sum=a[k];
	printf("该数组中第k大的数是 ：%d",sum);
	return 0;
}


*/




#include <iostream>
#include <math.h>
#include <cstdlib>

using namespace std;

int partition(int array[], size_t arrStart, size_t pivot, size_t arrEnd) {    //以pivot为中心点划分数组的函数
   int arrayLen = arrEnd - arrStart;

   if ( arrayLen < 1 || pivot < arrStart || pivot >= arrEnd) {
       cout << "Please check your implementation." << endl;
       return -1;
   }
   int pivotValue = array[pivot];    
   array[pivot] = array[arrEnd - 1];
   int pivotPos = arrStart;
   int temp;

   for (int i = arrStart; i < arrEnd - 1; ++i)
   {
       if (array[i] < pivotValue) {   //如果遍历到的数小于povitValue 
          temp = array[pivotPos];        //将其交换到前面 
          array[pivotPos] = array[i];
          array[i] = temp;
          ++pivotPos;                    //下标向后移一个单位 
       }
   }
   array[arrEnd - 1] = array[pivotPos];   
   array[pivotPos] = pivotValue;            //将pivotVaule交换到排序好的正确位置 

   return pivotPos;                      //将数返回到开始遍历的位置 
}
bool quickSort(int array[], size_t arrStart, size_t arrEnd) {
    int arrLen = arrEnd - arrStart;
    if (arrLen < 0) {
        cout << "Please check your input." << endl;
        return false;
    }

    if (arrLen == 0 || arrLen == 1) {
        return true;
    } 

    srand(array[array[arrStart] + arrLen + array[arrEnd - 1]]);

    int pivot = arrStart + floor(((arrLen - 1) + (size_t)rand()) % (arrLen - 1));   //生成数组中的随机数pivot 

    int pivotPos = partition(array, arrStart, pivot, arrEnd);  //以pivot为划分点   用partition函数划分数组 

    quickSort(array, arrStart, pivotPos);   //前半区排序 
    quickSort(array, pivotPos+1, arrEnd);   //后半区排序 

    return true;
}

void printArray(int array[], int arrLen) {
    for (int i = 0; i < arrLen; ++i) {
        cout << array[i] << " ";
    }
    cout << endl;
}

int main(){
    int array0[] = {};
    int arrayLen = sizeof(array0)/sizeof(int);

    printArray(array0, arrayLen);
    quickSort(array0, 0, arrayLen);
    printArray(array0, arrayLen);

    cout << "=========================================" << endl;

    int array1[] = {1};
    arrayLen = sizeof(array1)/sizeof(int);

    printArray(array1, arrayLen);
    quickSort(array1, 0, arrayLen);
    printArray(array1, arrayLen);

    cout << "=========================================" << endl;

    int array2[] = {2, 1};
    arrayLen = sizeof(array2)/sizeof(int);

    printArray(array2, arrayLen);
    quickSort(array2, 0, arrayLen);
    printArray(array2, arrayLen);

    cout << "=========================================" << endl;

    int array3[] = {1, 5, 3};
    arrayLen = sizeof(array3)/sizeof(int);

    printArray(array3, arrayLen);
    quickSort(array3, 0, arrayLen);
    printArray(array3, arrayLen);


    cout << "=========================================" << endl;

    int array4[] = {9, 12, 8, 7};
    arrayLen = sizeof(array4)/sizeof(int);

    printArray(array4, arrayLen);
    quickSort(array4, 0, arrayLen);
    printArray(array4, arrayLen);

    cout << "=========================================" << endl;

    int array5[] = {9, 12, 8, 7, 3};
    arrayLen = sizeof(array5)/sizeof(int);

    printArray(array5, arrayLen);
    quickSort(array5, 0, arrayLen);
    printArray(array5, arrayLen);


}